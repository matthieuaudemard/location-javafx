package fr.audemard.location.service;

import fr.audemard.location.persistence.entity.Adresse;
import fr.audemard.mycore.service.Service;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AdresseService implements Service {

    private static final String GEO_API = "https://api-adresse.data.gouv.fr/search/";

    public AdresseService() {
        // Aucune implémentation nécessaire.
    }

    /**
     * Recherche des adresses grâce à une API de géocodage externe.
     * @param street nom de la rue
     * @param postcode code postal de la ville
     * @return la liste des éléments correspondants renvoyés par l'apo de géocodage.
     */
    public List<Adresse> findAdresseByGeoApi(String street, String postcode) {
        // On vérifie la conformité des données
        if ((street == null || street.isEmpty()) && (postcode == null || postcode.isEmpty())) {
            return Collections.emptyList();
        }
        List<Adresse> result = new ArrayList<>();
        String query = buildGeoApiQuery(street, postcode);
        HttpGet request = new HttpGet(AdresseService.GEO_API + "?" + query);
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(request)) {
            // Vérification du statut HTTP 200 OK
            if (response.getStatusLine().getStatusCode() == 200) {
                HttpEntity responseEntity = response.getEntity();
                Header contentType = responseEntity.getContentType();
                // On vérifie que le contenu de la réponse est bien du JSON
                if (contentType != null && contentType.getValue().contains("application/json;")) {
                    String json = EntityUtils.toString(responseEntity);
                    // Conversion de la réponse en JSONObject
                    JSONObject jsonObject = new JSONObject(json);
                    // le champs features de la réponse correspond au tableau d'adresses envoyées par l'API
                    JSONArray jsonArray = jsonObject.getJSONArray("features");
                    // Construction de la liste de résultats
                    for (Object object : jsonArray) {
                        Adresse adresse = buildAdresseFromJson((JSONObject) object);
                        result.add(adresse);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Construit les arguments de la requête http formatés (ex: q=nom+de+rue&postcode=un+code+postal
     * @param street nom de la rue.
     * @param postcode code postal de la ville.
     * @return la chaîne de caractère formattée correspondant aux arguments de la requête http
     */
    private String buildGeoApiQuery(String street, String postcode) {
        String q = "";
        if (street != null && !street.isEmpty()) {
            q = "q=" + street.replace(' ','+');
        }
        String p = "";
        if (postcode != null && !postcode.isEmpty()) {
             p = "postcode=" + postcode;
        }
        return String.join("&", q, p);
    }

    /**
     * Construit un objet Adresse à partir d'un objet JSONObject
     * @param jsonObject JSONObject contenant les champs nécessaires à la création de l'Adresse
     * @return Adresse
     */
    private Adresse buildAdresseFromJson(JSONObject jsonObject) {
        Adresse result = new Adresse();
        // récupération des données contenu dans le champs properties de l'objet
        JSONObject properties = (JSONObject) jsonObject.get("properties");
        // récupère sous forme de tableau l'intitulé de tous les champs de properties
        // car suivant les réponses, certains champs ne sont pas envoyés par l'API.
        String[] names = JSONObject.getNames(properties);
        for (String name : names) {
            switch (name.toLowerCase()) {
                case "id":
                    result.setId(properties.getString(name));
                    break;
                case "label":
                    result.setLabel(properties.getString(name));
                    break;
                case "score":
                    result.setScore(properties.getDouble(name));
                    break;
                case "housenumber":
                    result.setHousenumber(properties.getString(name));
                    break;
                case "type":
                    result.setType(properties.getString(name));
                    break;
                case "name":
                    result.setName(properties.getString(name));
                    break;
                case "postcode":
                    result.setPostcode(properties.getString(name));
                    break;
                case "citycode":
                    result.setCitycode(properties.getString(name));
                    break;
                case "x":
                    result.setX(properties.getDouble(name));
                    break;
                case "y":
                    result.setX(properties.getDouble(name));
                    break;
                case "city":
                    result.setCity(properties.getString(name));
                    break;
                case "context":
                    result.setContext(properties.getString(name));
                    break;
                case "importance":
                    result.setImportance(properties.getDouble(name));
                    break;
                case "street":
                    result.setStreet(properties.getString(name));
                    break;
                default:
                    break;
            }
        }
        return result;
    }
}
