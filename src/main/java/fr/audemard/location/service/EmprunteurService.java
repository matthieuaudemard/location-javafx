package fr.audemard.location.service;

import fr.audemard.location.persistence.dao.EmprunteurDao;
import fr.audemard.location.persistence.entity.Emprunteur;
import fr.audemard.location.presentation.dataproperty.EmprunteurDataProperty;
import fr.audemard.mycore.persistence.factory.impl.DaoFactoryImpl;
import fr.audemard.mycore.service.Service;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.List;

public class EmprunteurService implements Service {

    private final EmprunteurDao emprunteurDao = new DaoFactoryImpl().getDao(EmprunteurDao.class);

    /**
     * Permet de rechercher tous les emprunteurs en base de données
     * @return l'ObservableList contenant tous les emprunteurs
     */
    public ObservableList<EmprunteurDataProperty> findAll() {
        ObservableList<EmprunteurDataProperty> emprunteurs = FXCollections.observableArrayList();
        List<Emprunteur> emprunteurList = emprunteurDao.findAll();

        for (Emprunteur emprunteur : emprunteurList) {
            EmprunteurDataProperty emprunteurProperty = new EmprunteurDataProperty();
            emprunteurProperty.setIdEmprunteur(emprunteur.getIdEmprunteur());
            emprunteurProperty.setNomEmprunteur(emprunteur.getNomEmprunteur());
            emprunteurProperty.setPrenomEmprunteur(emprunteur.getPrenomEmprunteur());
            emprunteurProperty.setEmailEmprunteur(emprunteur.getEmailEmprunteur());
            emprunteurProperty.setRueEmprunteur(emprunteur.getRueEmprunteur());
            emprunteurProperty.setZipCodeEmprunteur(emprunteur.getZipCodeEmprunteur());
            emprunteurProperty.setVilleEmprunteur(emprunteur.getVilleEmprunteur());
            emprunteurs.add(emprunteurProperty);
        }
        return emprunteurs;
    }

    /**
     * Supprime un emprunteur
     * @param emprunteurProperty représente l'emprunteur à supprimer
     */
    public void deleteEmprunteur(EmprunteurDataProperty emprunteurProperty) {
        Long idEmprunteur = emprunteurProperty.getIdEmprunteur();
        if (idEmprunteur != null) {
            Emprunteur emprunteur = emprunteurDao.findByPK(idEmprunteur);
            if (emprunteur != null) {
                emprunteurDao.delete(emprunteur);
            }
        }
    }

    /**
     * Persiste un emprunteur (create / update)
     * @param emprunteurProperty l'emprunteur à persister
     * @return l'emprunteur créé ou modifié
     */
    public EmprunteurDataProperty saveEmprunteur(EmprunteurDataProperty emprunteurProperty) {
        Emprunteur emprunteur = new Emprunteur();
        if (emprunteurProperty.getIdEmprunteur() != 0) {
            emprunteur.setIdEmprunteur(emprunteurProperty.getIdEmprunteur());
        }
        emprunteur.setNomEmprunteur(emprunteurProperty.getNomEmprunteur());
        emprunteur.setPrenomEmprunteur(emprunteurProperty.getPrenomEmprunteur());
        emprunteur.setEmailEmprunteur(emprunteurProperty.getEmailEmprunteur());
        emprunteur.setRueEmprunteur(emprunteurProperty.getRueEmprunteur());
        emprunteur.setZipCodeEmprunteur(emprunteurProperty.getZipCodeEmprunteur());
        emprunteur.setVilleEmprunteur(emprunteurProperty.getVilleEmprunteur());
        emprunteur = emprunteurDao.save(emprunteur);
        emprunteurProperty.setIdEmprunteur(emprunteur.getIdEmprunteur());

        return emprunteurProperty;
    }
}
