package fr.audemard.location.persistence.entity;

import fr.audemard.mycore.persistence.entity.BaseEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "EXEMPLAIRE")
public class Exemplaire implements BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "EX_ID")
    private Long exemplaireId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VEHICULE_ID")
    private Vehicule vehicule;

    @Column(name = "EX_IMMATRICULATION")
    private String immatriculation;

    @Column(name = "EX_KILOMETRAGE")
    private Double kilometrage;

    @OneToMany(mappedBy = "exemplaire")
    private List<Location> locations = new ArrayList<>();

    public Exemplaire() {
        // Toute classe persistante doit avoir un constructeur par défaut (qui peut être non-publique)
        // pour permettre à Hibernate de l'instancier en utilisant Constructor.newInstance()
    }

    @Override
    public Long getPrimaryKey() {
        return exemplaireId;
    }

    public Long getExemplaireId() {
        return getPrimaryKey();
    }

    public Vehicule getVehicule() {
        return vehicule;
    }

    public void setVehicule(Vehicule vehicule) {
        this.vehicule = vehicule;
    }

    public String getImmatriculation() {
        return immatriculation;
    }

    public void setImmatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
    }

    public Double getKilometrage() {
        return kilometrage;
    }

    public void setKilometrage(Double kilometrage) {
        this.kilometrage = kilometrage;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }
}
