package fr.audemard.location.persistence.entity;

import fr.audemard.mycore.persistence.entity.BaseEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "LOCATION")
public class Location implements BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "LOC_ID")
    private Long locationId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EX_ID")
    private Exemplaire exemplaire;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EMP_ID")
    private Emprunteur emprunteur;

    @Column(name = "LOC_DATE_RETRAIT", nullable = false)
    private Date locationDateRetrait;

    @Column(name = "LOC_DATE_RETOUR")
    private Date locationDateRetour;

    @Override
    public Long getPrimaryKey() {
        return null;
    }

    public Long getLocationId() {
        return getPrimaryKey();
    }

    public Exemplaire getExemplaire() {
        return exemplaire;
    }

    public void setExemplaire(Exemplaire exemplaire) {
        this.exemplaire = exemplaire;
    }

    public Emprunteur getEmprunteur() {
        return emprunteur;
    }

    public void setEmprunteur(Emprunteur emprunteur) {
        this.emprunteur = emprunteur;
    }

    public Date getLocationDateRetrait() {
        return locationDateRetrait;
    }

    public void setLocationDateRetrait(Date locationDateRetrait) {
        this.locationDateRetrait = locationDateRetrait;
    }

    public Date getLocationDateRetour() {
        return locationDateRetour;
    }

    public void setLocationDateRetour(Date locationDateRetour) {
        this.locationDateRetour = locationDateRetour;
    }
}
