package fr.audemard.location.persistence.entity;

import fr.audemard.mycore.persistence.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TYPE_VEHICULE")
public class TypeVehicule implements BaseEntity<String> {

    @Id
    @Column(name = "TYPE_CODE")
    private String typeVehiculeCode;

    @Column(name = "TYPE_LIBELLE")
    private String typeVehiculeLibelle;

    @Override
    public String getPrimaryKey() {
        return typeVehiculeCode;
    }

    public String getTypeVehiculeCode() {
        return getPrimaryKey();
    }

    public void setTypeVehiculeCode(String typeVehiculeCode) {
        this.typeVehiculeCode = typeVehiculeCode;
    }

    public String getTypeVehiculeLibelle() {
        return typeVehiculeLibelle;
    }

    public void setTypeVehiculeLibelle(String typeVehiculeLibelle) {
        this.typeVehiculeLibelle = typeVehiculeLibelle;
    }
}
