package fr.audemard.location.persistence.entity;

import fr.audemard.mycore.persistence.entity.BaseEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "EMPRUNTEUR")
public class Emprunteur implements BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "EMP_ID")
    private Long idEmprunteur;

    @Column(name = "EMP_NOM")
    private String nomEmprunteur;

    @Column(name = "EMP_PRENOM")
    private String prenomEmprunteur;

    @Column(name = "EMP_EMAIL")
    private String emailEmprunteur;

    @Column(name = "EMP_RUE")
    String rueEmprunteur;

    @Column(name = "EMP_ZIP_CODE")
    String zipCodeEmprunteur;

    @Column(name = "EMP_VILE")
    String villeEmprunteur;

    @OneToMany(mappedBy = "emprunteur")
    private List<Location> locations = new ArrayList<>();

    @Override
    public Long getPrimaryKey() {
        return idEmprunteur;
    }

    public void setIdEmprunteur(Long idEmprunteur) {
        this.idEmprunteur = idEmprunteur;
    }

    public Long getIdEmprunteur() {
        return getPrimaryKey();
    }

    public String getNomEmprunteur() {
        return nomEmprunteur;
    }

    public void setNomEmprunteur(String nomEmprunteur) {
        this.nomEmprunteur = nomEmprunteur;
    }

    public String getPrenomEmprunteur() {
        return prenomEmprunteur;
    }

    public void setPrenomEmprunteur(String prenomEmprunteur) {
        this.prenomEmprunteur = prenomEmprunteur;
    }

    public String getEmailEmprunteur() {
        return emailEmprunteur;
    }

    public void setEmailEmprunteur(String emailEmprunteur) {
        this.emailEmprunteur = emailEmprunteur;
    }

    public String getRueEmprunteur() {
        return rueEmprunteur;
    }

    public void setRueEmprunteur(String rueEmprunteur) {
        this.rueEmprunteur = rueEmprunteur;
    }

    public String getZipCodeEmprunteur() {
        return zipCodeEmprunteur;
    }

    public void setZipCodeEmprunteur(String zipCodeEmprunteur) {
        this.zipCodeEmprunteur = zipCodeEmprunteur;
    }

    public String getVilleEmprunteur() {
        return villeEmprunteur;
    }

    public void setVilleEmprunteur(String villeEmprunteur) {
        this.villeEmprunteur = villeEmprunteur;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }
}
