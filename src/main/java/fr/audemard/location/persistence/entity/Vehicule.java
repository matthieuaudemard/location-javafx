package fr.audemard.location.persistence.entity;


import fr.audemard.mycore.persistence.entity.BaseEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "VEHICULE")
public class Vehicule implements BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "VEHICULE_ID")
    private Long vehiculeId;

    @Column(name = "MARQUE")
    private String marque;

    @Column(name = "MODELE")
    private String modele;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TYPE_CODE", nullable = false)
    private TypeVehicule typeVehicule;

    @OneToMany(mappedBy = "vehicule")
    private List<Exemplaire> exemplaires = new ArrayList<>();

    public Long getPrimaryKey() {
        return vehiculeId;
    }

    public Long getVehiculeId() {
        return getPrimaryKey();
    }

    public TypeVehicule getTypeVehicule() {
        return typeVehicule;
    }

    public void setTypeVehicule(TypeVehicule typeVehicule) {
        this.typeVehicule = typeVehicule;
    }

    public void setVehiculeId(Long id) {
        this.vehiculeId = id;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public List<Exemplaire> getExemplaires() {
        return exemplaires;
    }

    public void setExemplaires(List<Exemplaire> exemplaires) {
        this.exemplaires = exemplaires;
    }
}
