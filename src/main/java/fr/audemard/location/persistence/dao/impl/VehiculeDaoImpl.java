package fr.audemard.location.persistence.dao.impl;

import fr.audemard.location.persistence.dao.VehiculeDao;
import fr.audemard.location.persistence.entity.Vehicule;
import fr.audemard.mycore.persistence.dao.impl.AbstractDaoImpl;

public class VehiculeDaoImpl extends AbstractDaoImpl<Vehicule, Long>
        implements VehiculeDao {
}
