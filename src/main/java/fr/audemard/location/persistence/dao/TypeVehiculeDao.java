package fr.audemard.location.persistence.dao;

import fr.audemard.location.persistence.entity.TypeVehicule;
import fr.audemard.mycore.persistence.dao.Dao;

public interface TypeVehiculeDao extends Dao<TypeVehicule, String> {
}
