package fr.audemard.location.persistence.dao;

import fr.audemard.location.persistence.entity.Vehicule;
import fr.audemard.mycore.persistence.dao.Dao;

public interface VehiculeDao extends Dao<Vehicule, Long> {

}
