package fr.audemard.location.persistence.dao.impl;

import fr.audemard.location.persistence.dao.ExemplaireDao;
import fr.audemard.location.persistence.entity.Exemplaire;
import fr.audemard.mycore.persistence.dao.impl.AbstractDaoImpl;

public class ExemplaireDaoImpl extends AbstractDaoImpl<Exemplaire, Long>
        implements ExemplaireDao {
}
