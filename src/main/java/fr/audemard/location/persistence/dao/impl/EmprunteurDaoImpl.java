package fr.audemard.location.persistence.dao.impl;

import fr.audemard.location.persistence.dao.EmprunteurDao;
import fr.audemard.location.persistence.entity.Emprunteur;
import fr.audemard.mycore.persistence.dao.impl.AbstractDaoImpl;

public class EmprunteurDaoImpl extends AbstractDaoImpl<Emprunteur, Long>
        implements EmprunteurDao {

}
