package fr.audemard.location.persistence.dao.impl;

import fr.audemard.location.persistence.dao.TypeVehiculeDao;
import fr.audemard.location.persistence.entity.TypeVehicule;
import fr.audemard.mycore.persistence.dao.impl.AbstractDaoImpl;

public class TypeVehiculeDaoImpl extends AbstractDaoImpl<TypeVehicule, String>
        implements TypeVehiculeDao {
}
