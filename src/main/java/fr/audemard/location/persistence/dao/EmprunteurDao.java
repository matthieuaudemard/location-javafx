package fr.audemard.location.persistence.dao;

import fr.audemard.location.persistence.entity.Emprunteur;
import fr.audemard.mycore.persistence.dao.Dao;

public interface EmprunteurDao extends Dao<Emprunteur, Long> {

}
