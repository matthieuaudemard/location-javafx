package fr.audemard.location.persistence.dao;

import fr.audemard.location.persistence.entity.Exemplaire;
import fr.audemard.mycore.persistence.dao.Dao;

public interface ExemplaireDao extends Dao<Exemplaire, Long> {
}
