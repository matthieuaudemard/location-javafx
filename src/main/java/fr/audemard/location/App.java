package fr.audemard.location;

import fr.audemard.location.presentation.controller.MainController;
import fr.audemard.location.presentation.viewmodel.MainDialogModel;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Hello world!
 *
 */
public class App extends Application {



    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("view/MainDialog.fxml"));
        MainController controller = new MainController(new MainDialogModel());
        loader.setController(controller);
        Parent root = loader.load();
        primaryStage.setTitle("Location 1.0");
        primaryStage.setOnCloseRequest(e -> {
            Platform.exit();
            System.exit(0);
        });
        primaryStage.setScene(new Scene(root));
        controller.initStage(primaryStage);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
