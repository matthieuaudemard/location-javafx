package fr.audemard.location.presentation.dataproperty;

import fr.audemard.mycore.presentation.model.DataProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class EmprunteurDataProperty implements DataProperty {

    private LongProperty idEmprunteur;
    private StringProperty nomEmprunteur;
    private StringProperty prenomEmprunteur;
    private StringProperty emailEmprunteur;
    private StringProperty rueEmprunteur;
    private StringProperty zipCodeEmprunteur;
    private StringProperty villeEmprunteur;

    public EmprunteurDataProperty() {
        idEmprunteur = new SimpleLongProperty();
        nomEmprunteur = new SimpleStringProperty();
        prenomEmprunteur = new SimpleStringProperty();
        emailEmprunteur = new SimpleStringProperty();
        rueEmprunteur = new SimpleStringProperty();
        zipCodeEmprunteur = new SimpleStringProperty();
        villeEmprunteur = new SimpleStringProperty();
    }

    public EmprunteurDataProperty(EmprunteurDataProperty item) {
        idEmprunteur = new SimpleLongProperty(item.getIdEmprunteur());
        nomEmprunteur = new SimpleStringProperty(item.getNomEmprunteur());
        prenomEmprunteur = new SimpleStringProperty(item.getPrenomEmprunteur());
        emailEmprunteur = new SimpleStringProperty(item.getEmailEmprunteur());
        rueEmprunteur = new SimpleStringProperty(item.getRueEmprunteur());
        zipCodeEmprunteur = new SimpleStringProperty(item.getZipCodeEmprunteur());
        villeEmprunteur = new SimpleStringProperty(item.getVilleEmprunteur());
    }

    public Long getIdEmprunteur() {
        return idEmprunteur.get();
    }

    public LongProperty idEmprunteurProperty() {
        return idEmprunteur;
    }

    public void setIdEmprunteur(long idEmprunteur) {
        this.idEmprunteur.set(idEmprunteur);
    }

    public String getNomEmprunteur() {
        return nomEmprunteur.get();
    }

    public StringProperty nomEmprunteurProperty() {
        return nomEmprunteur;
    }

    public void setNomEmprunteur(String nomEmprunteur) {
        this.nomEmprunteur.set(nomEmprunteur);
    }

    public String getPrenomEmprunteur() {
        return prenomEmprunteur.get();
    }

    public StringProperty prenomEmprunteurProperty() {
        return prenomEmprunteur;
    }

    public void setPrenomEmprunteur(String prenomEmprunteur) {
        this.prenomEmprunteur.set(prenomEmprunteur);
    }

    public String getEmailEmprunteur() {
        return emailEmprunteur.get();
    }

    public StringProperty emailEmprunteurProperty() {
        return emailEmprunteur;
    }

    public void setEmailEmprunteur(String emailEmprunteur) {
        this.emailEmprunteur.set(emailEmprunteur);
    }

    public String getRueEmprunteur() {
        return rueEmprunteur.get();
    }

    public StringProperty rueEmprunteurProperty() {
        return rueEmprunteur;
    }

    public void setRueEmprunteur(String rueEmprunteur) {
        this.rueEmprunteur.set(rueEmprunteur);
    }

    public String getZipCodeEmprunteur() {
        return zipCodeEmprunteur.get();
    }

    public StringProperty zipCodeEmprunteurProperty() {
        return zipCodeEmprunteur;
    }

    public void setZipCodeEmprunteur(String zipCodeEmprunteur) {
        this.zipCodeEmprunteur.set(zipCodeEmprunteur);
    }

    public String getVilleEmprunteur() {
        return villeEmprunteur.get();
    }

    public StringProperty villeEmprunteurProperty() {
        return villeEmprunteur;
    }

    public void setVilleEmprunteur(String villeEmprunteur) {
        this.villeEmprunteur.set(villeEmprunteur);
    }
}
