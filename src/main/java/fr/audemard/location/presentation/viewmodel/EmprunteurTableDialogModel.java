package fr.audemard.location.presentation.viewmodel;

import fr.audemard.location.presentation.dataproperty.EmprunteurDataProperty;
import fr.audemard.mycore.presentation.model.ViewModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class EmprunteurTableDialogModel implements ViewModel {

    private ObservableList<EmprunteurDataProperty> emprunteurs = FXCollections.observableArrayList();

    private ObservableList<String> filterChoice = FXCollections.observableArrayList("Id", "Nom", "Prénom");

    public ObservableList<EmprunteurDataProperty> getEmprunteurs() {
        return emprunteurs;
    }

    public void setEmprunteurs(ObservableList<EmprunteurDataProperty> emprunteurs) {
        this.emprunteurs = emprunteurs;
    }

    public ObservableList<String> getFilterChoice() {
        return filterChoice;
    }

    public void setFilterChoice(ObservableList<String> filterChoice) {
        this.filterChoice = filterChoice;
    }
}
