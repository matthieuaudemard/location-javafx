package fr.audemard.location.presentation.viewmodel;

import fr.audemard.location.presentation.dataproperty.EmprunteurDataProperty;
import fr.audemard.mycore.presentation.model.ViewModel;

public class EmprunteurEditorDialogModel implements ViewModel {

    private EmprunteurDataProperty emprunteur = new EmprunteurDataProperty();

    public EmprunteurDataProperty getEmprunteur() {
        return emprunteur;
    }

    public void setEmprunteur(EmprunteurDataProperty emprunteur) {
        this.emprunteur = emprunteur;
    }
}
