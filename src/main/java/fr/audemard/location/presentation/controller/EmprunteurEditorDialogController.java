package fr.audemard.location.presentation.controller;

import fr.audemard.location.persistence.entity.Adresse;
import fr.audemard.location.presentation.dataproperty.EmprunteurDataProperty;
import fr.audemard.location.presentation.viewmodel.EmprunteurEditorDialogModel;
import fr.audemard.location.service.AdresseService;
import fr.audemard.mycore.presentation.controller.impl.AbstractController;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.controlsfx.control.textfield.AutoCompletionBinding;
import org.controlsfx.control.textfield.TextFields;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmprunteurEditorDialogController extends AbstractController<EmprunteurEditorDialogModel> {

    @FXML
    private TextField villeEmprunteurText;

    @FXML
    private TextField zipcodeEmprunteurText;

    @FXML
    private TextField rueEmprunteurText;

    @FXML
    private TextField emailEmprunteurText;

    @FXML
    private TextField prenomEmprunteurText;

    @FXML
    private TextField nomEmprunteurText;

    @FXML
    private Button okButton;

    @FXML
    private Button cancelButton;

    private boolean okClicked = false;

    private AdresseService adresseService = new AdresseService();

    public EmprunteurEditorDialogController(EmprunteurEditorDialogModel model) {
        super(model);
    }

    public EmprunteurEditorDialogController() {
        this(new EmprunteurEditorDialogModel());
    }

    @Override
    protected void bindModelToProperties() {
        final EmprunteurDataProperty value = model.getEmprunteur();
        nomEmprunteurText.textProperty().bindBidirectional(value.nomEmprunteurProperty());
        prenomEmprunteurText.textProperty().bindBidirectional(value.prenomEmprunteurProperty());
        emailEmprunteurText.textProperty().bindBidirectional(value.emailEmprunteurProperty());
        rueEmprunteurText.textProperty().bindBidirectional(value.rueEmprunteurProperty());
        zipcodeEmprunteurText.textProperty().bindBidirectional(value.zipCodeEmprunteurProperty());
        villeEmprunteurText.textProperty().bindBidirectional(value.villeEmprunteurProperty());
    }

    @Override
    protected void finalizeBuilding() {
        implementAdresseAutocompletion();
    }

    private void implementAdresseAutocompletion() {
        // Liste des suggestions
        List<String> suggestions = new ArrayList<>();
        // Map qui associe à un label d
        Map<String, Adresse> adresseMap = new HashMap<>();
        AutoCompletionBinding<String> binding = TextFields.bindAutoCompletion(rueEmprunteurText, t -> {
            String address = t.getUserText();
            if (address.replace(" ", "").length() > 5) {
                List<Adresse> adresseByQuery = adresseService.findAdresseByGeoApi(address, zipcodeEmprunteurText.getText());
                adresseMap.clear();
                suggestions.clear();
                adresseByQuery.forEach(result -> {
                    String label = result.getLabel();
                    adresseMap.put(label, result);
                    suggestions.add(label);
                });
            }
            return suggestions;
        });
        binding.setOnAutoCompleted(v -> {
            String label = rueEmprunteurText.getText();
            if (label != null && !label.isEmpty()) {
                Adresse adresse = adresseMap.get(label);
                rueEmprunteurText.setText(adresse.getName());
                villeEmprunteurText.setText(adresse.getCity());
                zipcodeEmprunteurText.setText(adresse.getPostcode());
            }
        });
    }

    @FXML
    @Override
    protected void handleButtonEvent(ActionEvent actionEvent) {
        super.handleButtonEvent(actionEvent);
        // Evènement associé au bouton <Oui>
        if (actionEvent.getSource().equals(okButton)) {
            handleYes(actionEvent);
        }
        // Evènement associé au bouton <Non>
        else if (actionEvent.getSource().equals(cancelButton)) {
            handleNo(actionEvent);
        }
    }

    @Override
    protected void handleKeyEvent(KeyEvent event) {
        super.handleKeyEvent(event);
        // <CTRL-A> : selection de tout le texte si TextInputControl
        if (event.getCode() == KeyCode.ENTER) {
            handleYes(event);
        }
    }

    public boolean isOkClicked() {
        return okClicked;
    }

    private void handleYes(Event event) {
        okClicked = true;
        event.consume();
        getDialogStage().close();
    }

    private void handleNo(Event event) {
        event.consume();
        getDialogStage().close();
    }
}
