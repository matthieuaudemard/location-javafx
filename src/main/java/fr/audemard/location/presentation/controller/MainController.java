package fr.audemard.location.presentation.controller;

import fr.audemard.location.presentation.viewmodel.EmprunteurTableDialogModel;
import fr.audemard.location.presentation.viewmodel.MainDialogModel;
import fr.audemard.location.service.EmprunteurService;
import fr.audemard.mycore.presentation.controller.impl.AbstractController;
import fr.audemard.mycore.presentation.factory.BaseDialogFactory;
import fr.audemard.mycore.transverse.exception.AbstractException;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class MainController extends AbstractController<MainDialogModel> {

    @FXML
    private Button clientButton;

    @FXML
    private Button vehiculeButton;

    @FXML
    private Button exemplaireButton;

    @FXML
    private Button locationButton;

    private EmprunteurService emprunteurService = new EmprunteurService();

    public MainController(MainDialogModel model) {
        super(model);
    }

    @Override
    protected void bindModelToProperties() {
        // Pas d'implémentation nécessaire
    }

    @Override
    protected void finalizeBuilding() {

        clientButton.setOnAction(event -> {
            try {
                EmprunteurTableDialogModel model = new EmprunteurTableDialogModel();
                model.setEmprunteurs(emprunteurService.findAll());
                BaseDialogFactory.openDialog("view/emprunteur/EmprunteurTableDialog.fxml", EmprunteurTableDialogController.class, model, "Gestion des clients", true);
            } catch (AbstractException e) {
                e.printStackTrace();
            }
        });
    }
}
