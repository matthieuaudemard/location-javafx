package fr.audemard.location.presentation.controller;

import fr.audemard.location.presentation.dataproperty.EmprunteurDataProperty;
import fr.audemard.location.presentation.viewmodel.EmprunteurEditorDialogModel;
import fr.audemard.location.presentation.viewmodel.EmprunteurTableDialogModel;
import fr.audemard.location.service.EmprunteurService;
import fr.audemard.mycore.presentation.controller.impl.AbstractController;
import fr.audemard.mycore.presentation.factory.BaseDialogFactory;
import fr.audemard.mycore.transverse.exception.AbstractException;
import javafx.beans.binding.Bindings;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Font;

public class EmprunteurTableDialogController extends AbstractController<EmprunteurTableDialogModel> {

    private static final int UPDATE_EMPRUNTEUR = 0;
    private static final int CREATE_EMPRUNTEUR = 1;

    @FXML
    private Label titleLabel;

    @FXML
    private Button addButton;

    @FXML
    private Button editButton;

    @FXML
    private Button deleteButton;

    @FXML
    private Label nomEmprunteurValueLabel;

    @FXML
    private Label prenomEmprunteurValueLabel;

    @FXML
    private Label emailEmprunteurValueLabel;

    @FXML
    private Label rueEmprunteurValueLabel;

    @FXML
    private Label zipcodeEmprunteurValueLabel;

    @FXML
    private Label villeEmprunteurValeurLabel;

    @FXML
    private TextField filterField;

    @FXML
    private ChoiceBox<String> filterChoice;

    @FXML
    private TableView<EmprunteurDataProperty> tableEmprunteur;

    @FXML
    private TableColumn<EmprunteurDataProperty, Long> colEmprunteurId;

    @FXML
    private TableColumn<EmprunteurDataProperty, String> colNomEmprunteur;

    @FXML
    private TableColumn<EmprunteurDataProperty, String> colPrenomEmprunteur;

    private EmprunteurService emprunteurService;

    public EmprunteurTableDialogController(EmprunteurTableDialogModel model) {
        super(model);
        this.emprunteurService = new EmprunteurService();
    }

    public EmprunteurTableDialogController() {
        this(new EmprunteurTableDialogModel());
    }

    @Override
    protected void bindModelToProperties() {
        // Binding du tableEmprunteur
        final ObservableList<EmprunteurDataProperty> emprunteurListFromModel = model.getEmprunteurs();

        // Binding des colonnes
        colEmprunteurId.setCellValueFactory(new PropertyValueFactory<>("idEmprunteur"));
        colNomEmprunteur.setCellValueFactory(new PropertyValueFactory<>("nomEmprunteur"));
        colPrenomEmprunteur.setCellValueFactory(new PropertyValueFactory<>("prenomEmprunteur"));

        // Mise en place du filtre
        SortedList<EmprunteurDataProperty> sortedData = buildSortedList();
        tableEmprunteur.setItems(sortedData);

        filterChoice.setItems(model.getFilterChoice());
        filterChoice.getSelectionModel().selectFirst();

        final TableView.TableViewSelectionModel<EmprunteurDataProperty> selectionModel = tableEmprunteur.getSelectionModel();


        // MàJ du détail de l'emprunteur à la selection d'un nouvel item de la table
        selectionModel.selectedItemProperty().addListener((observable, oldValue, newValue) ->
            // MàJ de l'affichage du détail
            displayEmprunteur(newValue)
        );

        // Listener associé au double clique sur une ligne du table view
        tableEmprunteur.setOnMouseClicked(event -> {
            if (event.getClickCount() > 1) {
                EmprunteurDataProperty toEdit = openEmprunteurEditorDialog(EmprunteurTableDialogController.UPDATE_EMPRUNTEUR);
                if (toEdit != null) {
                    editEmprunteur(toEdit);
                }
            }
        });

        // Listener associé à l'ajout / suppression d'emprunteur
        emprunteurListFromModel.addListener((ListChangeListener<EmprunteurDataProperty>) listener -> {
            // AJOUT : sélectionner l'élément ajouté
            if (listener.next() && listener.wasAdded()) {
                selectionModel.select(listener.getFrom());
            }
            // SUPRRESION + LISTE VIDE : on efface la sélection
            else if (listener.wasRemoved() && emprunteurListFromModel.isEmpty()) {
                selectionModel.clearSelection();
            }
            // SUPPRESSION : sélection du 1er élément
            else if (listener.wasRemoved()) {
                selectionModel.select(0);
            }
        });

        // Gestion du grisage des boutons <Editer> et <Supprimer>
        editButton.disableProperty()
                .bind(Bindings.isEmpty(tableEmprunteur.getItems())
                        .or(Bindings.isEmpty(tableEmprunteur.getSelectionModel().getSelectedIndices())));
        deleteButton.disableProperty()
                .bind(Bindings.isEmpty(tableEmprunteur.getItems())
                        .or(Bindings.isEmpty(tableEmprunteur.getSelectionModel().getSelectedIndices())));
    }

    @Override
    protected void finalizeBuilding() {
        final boolean listEmpty = tableEmprunteur.getItems().isEmpty();
        if (!listEmpty) {
            tableEmprunteur.getSelectionModel().select(0);
            displayEmprunteur(tableEmprunteur.getSelectionModel().getSelectedItem());
        }
        deleteButton.setStyle("-fx-base: #f44336; -fx-text-fill: #ffffff;");
        addButton.setOnAction(this::handleButtonEvent);
        editButton.setOnAction(this::handleButtonEvent);
        deleteButton.setOnAction(this::handleButtonEvent);
        titleLabel.setFont(new Font(22));

    }

    @Override
    protected void handleButtonEvent(ActionEvent actionEvent) {
        // Evenement associé au bouton <Ajouter>
        if (actionEvent.getSource().equals(addButton)) {
            EmprunteurDataProperty toAdd = openEmprunteurEditorDialog(EmprunteurTableDialogController.CREATE_EMPRUNTEUR);
            if (toAdd != null) {
                toAdd = emprunteurService.saveEmprunteur(toAdd);
                model.getEmprunteurs().add(toAdd);
                tableEmprunteur.getSelectionModel().select(toAdd);
            }
        }
        // Evenement associé au bouton <Editer>
        else if (actionEvent.getSource().equals(editButton)) {
            EmprunteurDataProperty toEdit = openEmprunteurEditorDialog(EmprunteurTableDialogController.UPDATE_EMPRUNTEUR);
            if (toEdit != null) {
                editEmprunteur(toEdit);
            }
        }
        // Evenement associé au bouton <Supprimer>
        else if (actionEvent.getSource().equals(deleteButton)) {
            BaseDialogFactory.createYesNoDialog(Alert.AlertType.WARNING, "Demande de confirmation",
                    "Suppression d'emprunteur", "Voulez vous vraiment supprimer cette fiche emprunteur ?")
                    .showAndWait()
                    .filter(response -> response.getButtonData().equals(ButtonBar.ButtonData.YES))
                    .ifPresent(response -> deleteEmprunteur(tableEmprunteur.getSelectionModel().getSelectedItem()));
        }
    }

    private boolean filter(final EmprunteurDataProperty emprunteur, final String predicat) {
        // Si le filtre est vide ou contient moins de 3 caractères alors on ne filtre pas
        if (predicat == null || predicat.length() < 3) {
            return true;
        }

        String toEvaluate = null;
        final String lowerCaseFilter = predicat.toLowerCase();
        String criteria = filterChoice.getSelectionModel().getSelectedItem();
        if ("Id".equals(criteria)) {
            toEvaluate = String.valueOf(emprunteur.getIdEmprunteur());
        }
        else if ("Nom".equals(criteria)) {
            toEvaluate = emprunteur.getNomEmprunteur();
        }
        else if ("Prénom".equals(criteria)) {
            toEvaluate = emprunteur.getPrenomEmprunteur();
        }
        return toEvaluate != null && toEvaluate.toLowerCase().contains(lowerCaseFilter);
    }

    private SortedList<EmprunteurDataProperty> buildSortedList() {
        // 1. Wrap the ObservableList in a FilteredList (initially display all data).
        FilteredList<EmprunteurDataProperty> filteredData = new FilteredList<>(model.getEmprunteurs(), p -> true);

        // 2. Set the filter Predicate whenever the filter changes.
        filterField.textProperty().addListener((observable, oldValue, newValue) ->
                filteredData.setPredicate(emprunteur -> filter(emprunteur, newValue)));

        filterChoice.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) ->
                filteredData.setPredicate(emprunteur -> filter(emprunteur, filterField.getText())));

        // 3. Wrap the FilteredList in a SortedList.
        SortedList<EmprunteurDataProperty> sortedData = new SortedList<>(filteredData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(tableEmprunteur.comparatorProperty());
        return sortedData;
    }

    private void editEmprunteur(EmprunteurDataProperty toEdit) {
        // Mise à jour des données de l'élément sélectionné
        EmprunteurDataProperty selectedElement = model.getEmprunteurs().get(tableEmprunteur.getSelectionModel().getSelectedIndex());
        selectedElement.idEmprunteurProperty().set(toEdit.getIdEmprunteur());
        selectedElement.nomEmprunteurProperty().set(toEdit.getNomEmprunteur());
        selectedElement.prenomEmprunteurProperty().set(toEdit.getPrenomEmprunteur());
        selectedElement.emailEmprunteurProperty().set(toEdit.getEmailEmprunteur());
        selectedElement.rueEmprunteurProperty().set(toEdit.getRueEmprunteur());
        selectedElement.zipCodeEmprunteurProperty().set(toEdit.getZipCodeEmprunteur());
        selectedElement.villeEmprunteurProperty().set(toEdit.getVilleEmprunteur());
        // Persistance des modifications
        emprunteurService.saveEmprunteur(selectedElement);
        // MàJ de l'affichage du détail
        displayEmprunteur(selectedElement);
    }

    private void displayEmprunteur(final EmprunteurDataProperty property) {
        if (property != null) {
            nomEmprunteurValueLabel.textProperty().set(property.getNomEmprunteur());
            prenomEmprunteurValueLabel.textProperty().set(property.getPrenomEmprunteur());
            emailEmprunteurValueLabel.textProperty().set(property.getEmailEmprunteur());
            rueEmprunteurValueLabel.textProperty().set(property.getRueEmprunteur());
            zipcodeEmprunteurValueLabel.textProperty().set(property.getZipCodeEmprunteur());
            villeEmprunteurValeurLabel.textProperty().set(property.getVilleEmprunteur());
        } else {
            nomEmprunteurValueLabel.textProperty().set("");
            prenomEmprunteurValueLabel.textProperty().set("");
            emailEmprunteurValueLabel.textProperty().set("");
            rueEmprunteurValueLabel.textProperty().set("");
            zipcodeEmprunteurValueLabel.textProperty().set("");
            villeEmprunteurValeurLabel.textProperty().set("");
        }
    }

    private void deleteEmprunteur(EmprunteurDataProperty property) {
        if (property != null) {
            tableEmprunteur.getSelectionModel().clearSelection();
            model.getEmprunteurs().remove(property);
            emprunteurService.deleteEmprunteur(property);
        }
    }

    private EmprunteurDataProperty openEmprunteurEditorDialog(final int action) {
        final EmprunteurDataProperty selectedItem = tableEmprunteur.getSelectionModel().getSelectedItem();
        if (action == UPDATE_EMPRUNTEUR && selectedItem == null) {
            // TODO: Modifier avec la création d'une exception
            return null;
        }

        EmprunteurEditorDialogModel editorVo = new EmprunteurEditorDialogModel();
        String dialogTitle;

        if (action == UPDATE_EMPRUNTEUR) {
            // Il est nécessaire de cloner "selectedItem" car si on travaille directement sur cet objet, les champs
            // qui lui sont bindés sont mis à jour en temps réel lors de l'édition des champs
            editorVo.setEmprunteur(new EmprunteurDataProperty(selectedItem));
            dialogTitle = "Editer Emprunteur";
        } else {
            dialogTitle = "Ajouter Emprunteur";
        }

        // Création du controller
        EmprunteurEditorDialogController editorController;
        try {
            editorController = BaseDialogFactory.openDialog("view/emprunteur/EmprunteurEditorDialog.fxml",
                    EmprunteurEditorDialogController.class, editorVo, dialogTitle, true);
            if (editorController.isOkClicked()) {
                return editorVo.getEmprunteur();
            }
        } catch (AbstractException e) {
            Alert alert = BaseDialogFactory.createDialog(Alert.AlertType.ERROR, "Erreur", "", "Erreur grave");
            alert.show();
        }
        return null;
    }
}
